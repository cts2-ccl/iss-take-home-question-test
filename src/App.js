import { useMemo } from "react";
import "./App.css";
import { Chart } from "react-charts";
import rawData from "./example.json";
import {
  Card,
  CardContent,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";

const InfoCard = ({ title, units, value }) => (
  <Card>
    <CardContent>
      <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        {title}
      </Typography>
      <Typography sx={{ textAlign: "end" }} variant="h5">
        {value}
      </Typography>
      <Typography
        sx={{ textAlign: "end", fontSize: 14, my: -1 }}
        color="text.secondary"
        gutterBottom
      >
        {units}
      </Typography>
    </CardContent>
  </Card>
);

function App() {
  const primaryAxis = useMemo(() => ({ getValue: (d) => d.hour }), []);
  const secondaryAxes = useMemo(() => [{ getValue: (d) => d.temperature }], []);

  const primaryAxis2 = useMemo(() => ({ getValue: (d) => d.time }), []);
  const secondaryAxes2 = useMemo(
    () => [{ getValue: (d) => d.kWh, elementType: "area" }],
    []
  );

  const hourlyTempMatchedToEnergyDateMapper = (d) => ({ ...d, time: d.hour });
  const temperatureData = Object.keys(rawData.hourlyTemperatureReadings).map(
    (d) => ({
      label: d,
      data: rawData.hourlyTemperatureReadings[d].map(
        hourlyTempMatchedToEnergyDateMapper
      ),
    })
  );

  const temperatureTableData = [...Array(24).keys()].map((hour) => {
    const d = { hour };
    Object.keys(rawData.hourlyTemperatureReadings).forEach((room) => {
      d[room] = rawData.hourlyTemperatureReadings[room][hour].temperature;
    });
    return d;
  });

  console.log(temperatureTableData);
  const energyUsePerTimePeriod = rawData.energyReadings.map((d, i, a) => ({
    ...d,
    kWh: d.kWh - a[i ? i - 1 : 0].kWh,
  }));
  const energyUseFromStartOfPeriod = rawData.energyReadings.map((d, i, a) => ({
    ...d,
    kWh: d.kWh - a[0].kWh,
  }));
  const energyData = [
    { label: "current use", data: energyUsePerTimePeriod },
    { label: "total use", data: energyUseFromStartOfPeriod },
  ];

  const totalEnergyUse =
    energyUseFromStartOfPeriod[energyUseFromStartOfPeriod.length - 1].kWh;

  return (
    <div className="App" style={{display: "flex", justifyContent: "center"}}>
      <Grid container spacing={4} padding={6} maxWidth="lg">
        <Grid item xs={4}>
          <InfoCard
            units="kWh"
            value={totalEnergyUse.toFixed(3)}
            title="Total energy use"
          />
        </Grid>
        <Grid item xs={4}>
          <InfoCard
            units="£"
            value={(
              totalEnergyUse *
              rawData.costPerKilowattHourInPence *
              100
            ).toFixed(2)}
            title="Total cost"
          />
        </Grid>
        <Grid item xs={4}>
          <InfoCard
            units="£"
            value={rawData.costPerKilowattHourInPence * 100}
            title="Cost per kWh"
          />
        </Grid>

        <Grid item xs={12}>
          <Paper sx={{ height: 300 }}>
          <Paper sx={{ height: 300, p: 2 }}>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
              Energy usage
            </Typography>
            <div style={{height:"90%"}} >
              <Chart
                options={{
                  data: energyData,
                  primaryAxis: primaryAxis2,
                  secondaryAxes: secondaryAxes2,
                }}
              />
            </div>
          </Paper>
          </Paper>
        </Grid>

        <Grid item xs={12} mt={4}>
          <Paper sx={{ height: 300, p: 2 }}>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
              Room temperature
            </Typography>
            <div style={{height:"90%"}} >
              <Chart
                options={{ data: temperatureData, primaryAxis, secondaryAxes }}
                />
            </div>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow >
                  <TableCell sx={{fontWeight: 600}}>Hour</TableCell>
                  {Object.keys(rawData.hourlyTemperatureReadings).map(
                      (room) => (
                        <TableCell key={room} align="right" sx={{fontWeight: 600}}>{room}</TableCell>
                      )
                    )}
                </TableRow>
              </TableHead>
              <TableBody>
                {temperatureTableData.map((row) => (
                  <TableRow
                    key={row.hour}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.hour}
                    </TableCell>
                    {Object.keys(rawData.hourlyTemperatureReadings).map(
                      (room) => (
                        <TableCell key={room} component="th" scope="row" align="right">
                          {row[room].toFixed(1)}
                        </TableCell>
                      )
                    )}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
